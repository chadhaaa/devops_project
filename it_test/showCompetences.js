const expect = require('chai').expect
const request = require('request')
const server = require('../server')
let chai = require('chai')
let chaiHttp = require('chai-http')
let should = chai.should()

chai.use(chaiHttp)
describe('Competences API', () => {
	it('Should show Competences', (done) => {
		chai
			.request(server)
			.get('/api/competences')
			.end((err, res) => {
				res.should.have.status(200)
				done()
			})
	})
})
