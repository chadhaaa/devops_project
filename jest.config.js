module.exports = {
	collectCoverageFrom: ['./**/?(*.)(controller).{js,jsx,mjs}'],
	coverageReporters: ['html', 'text', 'text-summary', 'cobertura'],
	testMatch: ['./**/?(*.)(test).{js,jsx,mjs}'],
}
