const server = require('../server.js')
const supertest = require('supertest')
const { response } = require('../server.js')
const requestWithSupertest = supertest(server)

describe('Coach and Player Endpoints', () => {
	// test GET ALL COMPETENCES chadha_hajji_crudComp.js
	it('GET api/competences should show all competences', async () => {
		const res = await requestWithSupertest.get('/api/competences')
		expect(res.status).toEqual(200)
	})
	// test GET ALL STATISTICS chadha_hajji_crudStat.js
	it('GET api/statistics should show all statistics', async () => {
		const res = await requestWithSupertest.get('/api/statistics')
		expect(res.status).toEqual(200)
	})

	// test Invite Player chadha_hajji_invitePlayer.js
	it('POST /api/players should invite a player', async () => {
		const invitePlayer = {
			firstname: 'chadha',
			lastname: 'hajji',
			email: 'chadha.hadji@gmail.com',
			sessionPrice: '10',
			sessionNumbers: '3',
			password: '12345678',
		}
		const invitePlayerRes = await requestWithSupertest
			.post('/api/players')
			.send(invitePlayer)

		expect(invitePlayerRes.statusCode).toEqual(200)
		//const res = await requestWithSupertest.get(`/api/players/${invitePlayerRes.body._id}`)
		//expect(invitePlayerRes.body.email).toEqual('chadha.hadji@gmail.com')
	})
})
